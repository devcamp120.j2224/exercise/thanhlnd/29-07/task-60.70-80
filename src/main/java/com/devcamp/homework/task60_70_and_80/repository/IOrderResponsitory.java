package com.devcamp.homework.task60_70_and_80.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.homework.task60_70_and_80.model.COrder;

public interface IOrderResponsitory extends JpaRepository<COrder, Long>{
    
}

package com.devcamp.homework.task60_70_and_80.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.homework.task60_70_and_80.model.CCustomer;
import com.devcamp.homework.task60_70_and_80.model.COrder;
import com.devcamp.homework.task60_70_and_80.repository.ICustomerRepository;

@RequestMapping("/")
@RestController
@CrossOrigin(value = "*" , maxAge =  -1)
public class CustomerController {
    @Autowired
    ICustomerRepository iCustomerRepository;
    @GetMapping("/devcamp_customer")
    public ResponseEntity<List<CCustomer>> getCustomerList(){
        try{
            List<CCustomer> listCustomer = new ArrayList<CCustomer>();

            iCustomerRepository.findAll().forEach(listCustomer::add);
            return new ResponseEntity<List<CCustomer>>(listCustomer, HttpStatus.OK);
        }catch(Exception ex){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/devcamp-orders")
    public ResponseEntity<Set<COrder>> getOrderList(@RequestParam (value = "userId") Integer order){
        try{
            CCustomer vCustomer = iCustomerRepository.findById(order);
            if(vCustomer != null){
                return new ResponseEntity<>(vCustomer.getcOrders(), HttpStatus.OK);
            }else{
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        }catch(Exception ex){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

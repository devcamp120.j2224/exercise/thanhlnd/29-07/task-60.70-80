package com.devcamp.homework.task60_70_and_80;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task6070And80Application {

	public static void main(String[] args) {
		SpringApplication.run(Task6070And80Application.class, args);
	}

}
